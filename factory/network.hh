namespace factory {
	
	namespace components {

		template<class T>
		struct base_format {
			base_format(T value): _value(value) {}
			operator T() const { return _value; }
		protected:
			T _value;
		};

		template<class T, size_t bytes>
		struct network_format: public base_format<T> {
			network_format(T value): base_format<T>(value) {}
			operator T() const {
#if __BYTE_ORDER == __LITTLE_ENDIAN
				const char* raw = reinterpret_cast<const char*>(&(base_format<T>::_value));
				T newval;
				std::reverse_copy(raw, raw + bytes, (char*)&newval);
				return newval;
#else
				return base_format<T>::_value;
#endif
			}
		};

		template<class T>
		struct network_format<T, 1>: public base_format<T> {
			network_format(T value): base_format<T>(value) {}
			operator T() const { return base_format<T>::_value; }
		};

		template<class T>
		struct network_format<T, 2>: public base_format<T> {
			network_format(T value): base_format<T>(value) {}
			operator T() const { return htobe16(base_format<T>::_value); }
		};

		template<class T>
		struct network_format<T, 4>: public base_format<T> {
			network_format(T value): base_format<T>(value) {}
			operator T() const { return htobe32(base_format<T>::_value); }
		};

		template<class T>
		struct network_format<T, 8>: public base_format<T> {
			network_format(T value): base_format<T>(value) {}
			operator T() const { return htobe64(base_format<T>::_value); }
		};

		template<class T, size_t bytes, bool iec559 = std::numeric_limits<T>::is_iec559>
		struct network_format_fp: public base_format<T> {

#ifndef IGNORE_ISO_IEC559
			static_assert(iec559, "This system does not support ISO IEC 559"
			" floating point representation for either float, double or long double"
			" types, i.e. there is no portable way of"
			" transmitting floating point numbers over the network"
			" without precision loss. If all computers in the network do not"
			" conform to this standard but represent floating point"
			" numbers exactly in the same way, you can ignore this assertion"
			" by defining IGNORE_ISO_IEC559.");
#endif

			network_format_fp(T value): base_format<T>(value) {}

			operator T() const {
				const char* raw = reinterpret_cast<const char*>(&(base_format<T>::_value));
				T newval;
				std::reverse_copy(raw, raw + bytes, (char*)&newval);
				return newval;
			}
		};

		template<class T>
		struct network_format_fp<T, 1, true>: public base_format<T> {
			network_format_fp(T value): base_format<T>(value) {}
			operator T() const { return base_format<T>::_value; }
		};

		template<class T>
		struct network_format_fp<T, 2, true>: public base_format<T> {
			network_format_fp(T value): base_format<T>(value) {}
			operator T() const {
				return htobe16(*reinterpret_cast<const uint16_t*>(&(base_format<T>::_value)));
			}
		};

		template<class T>
		struct network_format_fp<T, 4, true>: public base_format<T> {
			network_format_fp(T value): base_format<T>(value) {}
			operator T() const {
				return htobe32(*reinterpret_cast<const uint32_t*>(&(base_format<T>::_value)));
			}
		};

		template<class T>
		struct network_format_fp<T, 8, true>: public base_format<T> {
			network_format_fp(T value): base_format<T>(value) {}
			operator T() const {
				return htobe64(*reinterpret_cast<const uint64_t*>(&(base_format<T>::_value)));
			}
		};

		// -----------
		// Host format
		// -----------

		template<class T, size_t bytes>
		struct host_format: public base_format<T> {
			host_format(T value): base_format<T>(value) {}
			operator T() const {
#if __BYTE_ORDER == __LITTLE_ENDIAN
				const char* raw = reinterpret_cast<const char*>(&(base_format<T>::_value));
				T newval;
				std::reverse_copy(raw, raw + bytes, (char*)&newval);
				return newval;
#else
				return base_format<T>::_value;
#endif
			}
		};

		template<class T>
		struct host_format<T, 1>: public base_format<T> {
			host_format(T value): base_format<T>(value) {}
			operator T() const { return base_format<T>::_value; }
		};

		template<class T>
		struct host_format<T, 2>: public base_format<T> {
			host_format(T value): base_format<T>(value) {}
			operator T() const { return be16toh(base_format<T>::_value); }
		};

		template<class T>
		struct host_format<T, 4>: public base_format<T> {
			host_format(T value): base_format<T>(value) {}
			operator T() const { return be32toh(base_format<T>::_value); }
		};

		template<class T>
		struct host_format<T, 8>: public base_format<T> {
			host_format(T value): base_format<T>(value) {}
			operator T() const { return be64toh(base_format<T>::_value); }
		};

		template<class T, size_t bytes, bool iec559 = std::numeric_limits<T>::is_iec559>
		struct host_format_fp: public base_format<T> {

#ifndef IGNORE_ISO_IEC559
			static_assert(iec559, "This system does not support ISO IEC 559"
			" floating point representation for either float, double or long double"
			" types, i.e. there is no portable way of"
			" transmitting floating point numbers over the network"
			" without precision loss. If all computers in the network do not"
			" conform to this standard but represent floating point"
			" numbers exactly in the same way, you can ignore this assertion"
			" by defining IGNORE_ISO_IEC559.");
#endif

			host_format_fp(T value): base_format<T>(value) {}

			operator T() const {
				const char* raw = reinterpret_cast<const char*>(&(base_format<T>::_value));
				T newval;
				std::reverse_copy(raw, raw + bytes, (char*)&newval);
				return newval;
			}
		};

		template<class T>
		struct host_format_fp<T, 1, true>: public base_format<T> {
			host_format_fp(T value): base_format<T>(value) {}
			operator T() const { return base_format<T>::_value; }
		};

		template<class T>
		struct host_format_fp<T, 2, true>: public base_format<T> {
			host_format_fp(T value): base_format<T>(value) {}
			operator T() const {
				return be16toh(*reinterpret_cast<const uint16_t*>(&(base_format<T>::_value)));
			}
		};

		template<class T>
		struct host_format_fp<T, 4, true>: public base_format<T> {
			host_format_fp(T value): base_format<T>(value) {}
			operator T() const {
				return be32toh(*reinterpret_cast<const uint32_t*>(&(base_format<T>::_value)));
			}
		};

		template<class T>
		struct host_format_fp<T, 8, true>: public base_format<T> {
			host_format_fp(T value): base_format<T>(value) {}
			operator T() const {
				return be64toh(*reinterpret_cast<const uint64_t*>(&(base_format<T>::_value)));
			}
		};
	
	}

	template<class T>
	T host_format(T value) {
		typedef typename
			std::conditional<std::is_floating_point<T>::value, 
				components::host_format_fp<T, sizeof(T)>,
				components::host_format<T, sizeof(T)>>::type
					Convert;
		return Convert(value);
	}

	template<class T>
	T network_format(T value) {
		typedef typename
			std::conditional<std::is_floating_point<T>::value, 
				components::network_format_fp<T, sizeof(T)>,
				components::network_format<T, sizeof(T)>>::type
					Convert;
		return Convert(value);
	}

}
