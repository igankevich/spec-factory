namespace factory {

	namespace configuration {

		typedef factory::components::Reflecting_kernel<
			factory::components::Mobile<
				factory::components::Kernel>>
					Kernel;

		typedef factory::components::Type<Kernel> Type;

		typedef factory::components::Kernel_pair<Kernel> Kernel_pair;

		typedef factory::components::Service<Kernel> Service;

		typedef factory::components::Scribe<Kernel> Scribe;

		typedef factory::components::Reader<Kernel> Reader;

		template<class T>
			using Mobile_init = factory::components::Mobile_init<T, Type, Kernel>;

		template<class K>
			using Identifiable = factory::components::Identifiable<K, Type>;
	
		template<class K>
			using Server = factory::components::Server<K>;

		typedef factory::components::Server_server<Service, Server> Server_server;

		template<class X> using Pool = std::queue<X>;

		template<class K, class U>
			using Rserver = factory::components::Rserver<Pool, K, Server, U>;

		template<class K, class D>
			using Downstream_server = factory::components::Downstream_server<Pool, K, Server, D>;
	
		template<class K, class S>
			using Iserver = factory::components::Iserver<K, Server, S>;

		template<class K, class U>
			using Remote_server = factory::components::Remote_server<K, U, Server, Type>;

		template<class K, class S>
			using Remote_Iserver = factory::components::Remote_Iserver<Pool, K, Server, S>;

		typedef
			factory::components::Server_stack<Kernel, Server,
				factory::components::Server_stack<Kernel_pair, Server,
					factory::components::Server_stack<Reader, Server,
						factory::components::Server_stack<Scribe, Server,
							factory::components::Server_stack<Service, Server,
								factory::components::Server_stack_end>>>>>
									Server_stack;

		typedef factory::components::Tetris Tetris;
	
		typedef factory::components::Stochastic_round_robin Stochastic_round_robin;
	
		typedef factory::components::Round_robin Round_robin;
	
		typedef factory::components::Simple_hashing Simple_hashing;

		template<class Base>
			using Resource_aware = factory::components::Resource_aware<Base, Remote_server>;
	
	}

}
