namespace factory {

	typedef struct epoll_event Event;
	
	class Event_poller {
	public:
	
		explicit Event_poller(Socket socket):
			_epollfd(check("epoll_create()", ::epoll_create(1))),
			_listener_socket(socket),
			_stopped(false),
			_mgmt_pipe()
		{
			add_service_socket(socket);
			add_service_socket(_mgmt_pipe.read_end());
		}
	
		~Event_poller() { ::close(_epollfd); }
	
		template<class Callback>
		void run(Callback callback) {
			while (!stopped()) this->wait(callback);
		}

		bool stopped() const { return _stopped; }

		void stop() {
			char s = 's';
			::write(_mgmt_pipe.write_end(), &s, 1);
		}
	
	private:
		void add_service_socket(int socket) {
			register_socket(socket, EPOLLIN);
		}
	
		void add_client_socket(int socket) {
			register_socket(socket, EPOLLIN | EPOLLET | EPOLLRDHUP);
		}
	
		void register_socket(int socket, int _events) {
			Event ev;
			ev.events = _events;
			ev.data.fd = socket;
			check("epoll_ctl()", ::epoll_ctl(_epollfd, EPOLL_CTL_ADD, socket, &ev));
		}
	
		template<class Callback>
		void wait(Callback callback) {
			int nfds = check("epoll_wait()", ::epoll_wait(_epollfd, _events, MAX_EVENTS, -1));
			for (int n=0; n<nfds; ++n) {
				if (_events[n].data.fd == _listener_socket) {
					Socket client_sock = _listener_socket.accept();
					client_sock.flags(O_NONBLOCK);
					add_client_socket(client_sock);
					std::cout << "Accepted connection from " << client_sock << std::endl;
				} else if (_events[n].data.fd == _mgmt_pipe.read_end()) {
					_stopped = true;
				} else {
					callback(_events[n]);
				}
			}
		}
	
		int _epollfd;
		Socket _listener_socket;
		bool _stopped;

		union Pipe {

			Pipe() {
				::pipe(_fds);
				int flags = ::fcntl(read_end(), F_GETFL);
				::fcntl(read_end(), F_SETFL, flags | O_NONBLOCK);
			}

			~Pipe() {
				::close(_fds[0]);
				::close(_fds[1]);
			}

			int read_end() const { return _fds[0]; }
			int write_end() const { return _fds[1]; }

		private:
			struct {
				int _read_fd;
				int _write_fd;
			} _unused;
			int _fds[2];

		} _mgmt_pipe;
	
		static const int MAX_EVENTS = 128;
		Event _events[MAX_EVENTS];
	};

}
