#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <ctime>
#include <vector>
#include <atomic>
#include <stdexcept>
#include <system_error>
#include <queue>
#include <algorithm>
#include <iterator>
#include <chrono>

// Metadata.
#include <string>
#include <functional>
#include <unordered_map>
#include <map>
#include <typeindex>
#include <type_traits>

// Standard portable types.
#include <cinttypes>
#include <cstring>

// C++ threads.
#include <thread>
#include <mutex>
#include <condition_variable>

// Linux (and hopefully POSIX) system
// Optimise calls to htons and htonl
#define __OPTIMIZE__
#include <unistd.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>

// Base system
#include "error.hh"
#include "network.hh"
#include "datastream.hh"
#include "socket.hh"
#include "event_poller.hh"
#include "endpoint.hh"
#include "type.hh"
#include "resource.hh"
#include "base.hh"
#include "kernel.hh"
#include "server.hh"
#include "strategy.hh"
#include "tetris.hh"
#include "mobile_init.hh"

// Aspects and their configuration
#ifdef LOG_SYSTEM_EVENTS
#include "journaling.hh"
#include "journaling_configuration.hh"
#else
#include "release_configuration.hh"
#endif

// Derived system
//#include "servers.hh"
#include "factory_instance.hh"
#include "kernels.hh"

