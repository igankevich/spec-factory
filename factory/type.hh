namespace factory {

	typedef uint32_t Id;

	const Id ROOT_ID = 0;

	namespace components {

		template<class K>
		class Type {
		public:

			/// A portable type id
			typedef int16_t Type_id;
	
			Type(): type(typeid(K)) {}
	
			Type(const Type& rhs):
				_id(rhs._id),
				type(rhs.type),
				construct(rhs.construct),
				read_object(rhs.read_object),
				write_object(rhs.write_object)
				{}

			Type_id id() const { return _id; }
			void id(Type_id i) { _id = i; }
	
			std::type_index type;
			std::function<K* ()> construct;
			std::function<void (Foreign_stream& in, K* rhs)> read_object;
			std::function<void (Foreign_stream& out, const K* rhs)> write_object;
	
			friend std::ostream& operator<<(std::ostream& out, const Type<K>& rhs) {
				return out << rhs.type.name();
			}

			class Types {
			public:
	
				typedef Type<K> T;
	
				const T* lookup(Type_id type_name) const {
					auto result = _types_by_id.find(type_name);
					return result == _types_by_id.end() ? nullptr : result->second; 
				}
		
				const T* lookup(const std::type_index& type_idx) const {
					auto result = types_by_index.find(type_idx);
					return result == types_by_index.end() ? nullptr : result->second; 
				}
		
				K* read_object(Foreign_stream& packet) const {
					K* object = nullptr;
					Type_id id;
					packet >> id;
					const T* type = lookup(id);
					if (type == nullptr) {
						std::stringstream msg;
						msg << "Demarshalling of non-kernel object with typeid = " << id << " was prevented.";
						throw Marshalling_error(msg.str(), __FILE__, __LINE__, __func__);
					}
					try {
						object = type->construct();
						type->read_object(packet, object);
					} catch (std::bad_alloc& err) {
						std::stringstream msg;
						msg << "Allocation error. Demarshalled kernel was prevented"
							" from allocating too much memory. " << err.what();
						throw Marshalling_error(msg.str(), __FILE__, __LINE__, __func__);
					}
					return object;
				}
	
				void register_type(T* type) {
					const T* existing_type = lookup(type->id());
					if (existing_type != nullptr) {
						std::stringstream msg;
						msg << "'" << *type << "' and '" << *existing_type
							<< "' have the same type identifiers.";
						throw Error(msg.str(), __FILE__, __LINE__, __func__);
					}
					_types_by_id[type->id()] = type;
					types_by_index[type->type] = type;
				}
		
				friend std::ostream& operator<<(std::ostream& out, const Types& rhs) {
					for (auto it=rhs._types_by_id.cbegin(); it!=rhs._types_by_id.cend(); ++it) {
						out << it->first << " -> " << *it->second << std::endl;
					}
					return out;
				}
		
			private:

				std::unordered_map<int16_t, T*> _types_by_id;
				std::unordered_map<std::type_index, T*> types_by_index;
			};

			class Instances {

			public:

				Instances(): _instances() {}

				K* lookup(Id id) const {
					auto result = _instances.find(id);
					return result == _instances.end() ? nullptr : result->second; 
				}

				void register_instance(K* inst) {
					_instances[inst->id()] = inst;
				}
		
				friend std::ostream& operator<<(std::ostream& out, const Instances& rhs) {
					// TODO: this function is not thread-safe
					for (auto it=rhs._instances.cbegin(); it!=rhs._instances.cend(); ++it) {
						out << it->first << " -> " << typeid(*it->second).name() << std::endl;
					}
					return out;
				}

			private:
				std::unordered_map<Id, K*> _instances;
			};

			static Types& types() {
				static Types x;
				return x;
			}
			
			static Instances& instances() {
				static Instances x;
				return x;
			}

		private:

			Type_id _id;
	
		};

		template<class Sub, class Type, class M>
		class Type_init: public M {
		public:
			const Type* type() const { return &_type; }

		private:
			struct Init: public Type {
				Init() {
					this->construct = [] { return new Sub(); };
					this->read_object = [] (Foreign_stream& in, M* rhs) { rhs->read(in); };
					this->write_object = [] (Foreign_stream& out, const M* rhs) { rhs->write(out); };
					this->type = typeid(Sub);
					Sub::init_type(this);
					try {
						Type::types().register_type(this);
					} catch (std::exception& err) {
						std::clog << "Error during initialisation of types. " << err.what() << std::endl;
						std::abort();
					}
				}
			};

			// Static template members are initialised on demand,
			// i.e. only if they are accessed in a program.
			// This function tries to fool the compiler.
			virtual Id unused() { return _type.id(); }

			static const Init _type;
		};

		template<class Sub, class Type, class M>
		const typename Type_init<Sub, Type, M>::Init Type_init<Sub, Type, M>::_type;

		template<class K, class Type>
		class Identifiable: public K {

		public:

			explicit Identifiable(Id i): _id(i) {}

			Identifiable(): _id(generate_id()) {
				Type::instances().register_instance(this);
				std::clog << typeid(*this).name() << "::id = " << id() << std::endl;
			}

			Id id() const { return _id; }
			void id(Id i) { _id = i; }

			Endpoint from() const { return _from; }
			void from(Endpoint endp) { _from = endp; }

		private:

			Id generate_id() {
				static std::atomic<Id> counter(start_id());
				return counter++;
			}

			static Id start_id() {
				const char* id = ::getenv("START_ID");
				Id i = 1;
				if (id != NULL) {
					std::stringstream tmp;
					tmp << id;
					if (!(tmp >> i) || i == ROOT_ID) {
						i = 1;
						std::clog << "Bad START_ID value: " << id << std::endl;
					}
				}
				std::clog << "START_ID = " << i << std::endl;
				return i;
			}

			Id _id;
			Endpoint _from;
		};
	
	}

}
