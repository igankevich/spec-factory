namespace factory {
	
	namespace components {

		template<class Sub, class Type, class M>
		class Mobile_init: public Kernel_link<Sub, Identifiable<M, Type>> {
		public:
			const Type* type() const { return &_type; }

		private:
			struct Init: public Type {
				Init() {
					this->construct = [] { return new Sub(); };
					this->read_object = [] (Foreign_stream& in, M* rhs) { rhs->read(in); };
					this->write_object = [] (Foreign_stream& out, const M* rhs) { rhs->write(out); };
					this->type = typeid(Sub);
					Sub::init_type(this);
					try {
						Type::types().register_type(this);
					} catch (std::exception& err) {
						std::clog << "Error during initialisation of types. " << err.what() << std::endl;
						std::abort();
					}
				}
			};

			// Static template members are initialised on demand,
			// i.e. only if they are accessed in a program.
			// This function tries to fool the compiler.
			virtual Id unused() { return _type.id(); }

			static const Init _type;
		};

		template<class Sub, class Type, class M>
		const typename Mobile_init<Sub, Type, M>::Init Mobile_init<Sub, Type, M>::_type;

	}
}
