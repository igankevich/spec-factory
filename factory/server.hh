#ifdef __linux__
// Locking thread to a single CPU.
#include <sched.h>
// Getting number of CPUs.
#include <unistd.h>
namespace {
	int total_cpus() { return ::sysconf(_SC_NPROCESSORS_ONLN); }
//	int thread_affinity() { return ::sched_getcpu(); }
	void thread_affinity(int cpu) {
		if (cpu != -1) {
			int num_cpus = total_cpus();
			::cpu_set_t* cpuset = CPU_ALLOC(num_cpus);
			std::size_t cpuset_size = CPU_ALLOC_SIZE(num_cpus);
			CPU_SET_S(cpu%num_cpus, cpuset_size, cpuset);
			::sched_setaffinity(0, cpuset_size, cpuset);
			CPU_FREE(cpuset);
		}
	}
}
#endif

#ifdef __sun__
#include <sys/processor.h>
// Getting number of CPUs.
#include <unistd.h>
namespace {
	void thread_affinity(int) {
//		int num_cpus = sysconf(_SC_NPROCESSORS_ONLN);
//		processor_bind(P_LWPID, P_MYID, cpu_id%num_cpus, NULL);
	}
	int thread_affinity() {
		processorid_t id;
		processor_bind(P_LWPID, P_MYID, PBIND_QUERY, &id);
		return id;
	}
	int total_cpus() { return ::sysconf(_SC_NPROCESSORS_ONLN); }
}
#endif

namespace {

	int total_threads() {
		const char* threads = ::getenv("NUM_THREADS");
		int t = total_cpus();
		if (threads != NULL) {
			std::stringstream tmp;
			tmp << threads;
			if (!(tmp >> t) || t < 0 || t > total_cpus()) {
				t = total_cpus();
				std::clog << "Bad NUM_THREADS value: " << threads << std::endl;
			}
		}
		std::clog << "threads = " <<  t << std::endl;
		return t;
	}

	int total_vthreads() {
		const char* threads = ::getenv("NUM_VTHREADS");
		int t = 1;
		if (threads != NULL) {
			std::stringstream tmp;
			tmp << threads;
			if (!(tmp >> t) || t < 0) {
				t = 1;
				std::clog << "Bad NUM_VTHREADS value: " << threads << std::endl;
			}
		}
		return t;
	}

}


namespace factory {

	namespace components {

		template<class T>
		struct Part_of_hierarchy {
			Part_of_hierarchy(): _parent(nullptr) {}
			T* parent() { return _parent; }
			void parent(T* s) { _parent = s; }
		private:
			T* _parent;
		};

		struct Resident {
		public:
			Resident(): _stopped(false) {}

			bool stopped() const { return _stopped; }
			void stopped(bool b) { _stopped = b; }

			virtual void wait() {}
			virtual bool stop() {
				bool b = !stopped();
				if (b) { stopped(true); }
				return b;
			}

		protected:
			virtual void wait_impl() = 0;
			virtual void stop_impl() = 0;

		private:
			bool _stopped;
		};

		template<class Sub, class Super>
		struct Server_link: public Super {
			void wait() {
				Super::wait();
				static_cast<Sub*>(this)->Sub::wait_impl();
			}
			bool stop() {
				bool ret = Super::stop();
				if (ret) {
					static_cast<Sub*>(this)->Sub::stop_impl();
				}
				if (Super::parent() != nullptr) {
					Super::parent()->stop();
				}
				return ret;
			}
		};

		template<class K>
		class Server:
			public virtual Resident,
			public Part_of_hierarchy<Server<K>>
		{
		public:
			using Part_of_hierarchy<Server<K>>::parent;

			Server() {}
			virtual ~Server() {}
			Server(const Server<K>&) = delete;
			void operator=(Server<K>&) = delete;

			virtual void send(K* kernel) = 0;
			virtual void* profiler() = 0;

			Server<K>* root() { return parent() == nullptr ? this : parent()->root(); }

		protected:
			friend std::ostream& operator<<(std::ostream& out, const Server<K>& rhs) {
				rhs.write(out); return out;
			}
			friend std::ostream& operator<<(std::ostream& out, const Server<K>* rhs) {
				rhs->write(out); return out;
			}
			virtual void write(std::ostream& out) const = 0;
		};



		template<class K, template<class X> class Server, class S>
		class Iserver: public Server_link<Iserver<K, Server, S>, Server<K>> {

		public:
			typedef typename S::Profiler  Profiler;
			typedef typename S::Iprofiler Iprofiler;

			explicit Iserver(const std::vector<Server<K>*>& upstream):
				_upstream(upstream),
				_strategy(upstream),
				_profiler(create_profilers(upstream))
			{
				std::for_each(_upstream.begin(), _upstream.end(), [this] (Server<K>* srv) {
					srv->parent(this);
				});
			}

		private:

			template<class Upstream>
			std::vector<Profiler*> create_profilers(const Upstream& upstream) const {
				std::vector<Profiler*> profilers(upstream.size());
				std::transform(_upstream.cbegin(), _upstream.cend(), profilers.begin(), [] (Server<K>* srv) {
					return static_cast<Profiler*>(srv->profiler());
				});
				return profilers;
			}

		public:

			~Iserver() {
				std::for_each(_upstream.begin(), _upstream.end(), [] (Server<K>* srv) {
					delete srv;
				});
			}

			void* profiler() { return &_profiler; }

			void send(K* a) {
				int i = _strategy(a, _profiler);
				_upstream[i]->send(a);
			}

			void wait_impl() {
				std::for_each(_upstream.begin(), _upstream.end(), std::mem_fn(&Server<K>::wait));
			}

			void stop_impl() {
				std::for_each(_upstream.begin(), _upstream.end(), std::mem_fn(&Server<K>::stop));
			}

			void write(std::ostream& out) const {
				out << "iserver {";
				std::ostream_iterator<Server<K>*> out_it(out, ";");
				std::copy(_upstream.begin(), _upstream.end(), out_it);
				out << '}';
			}

		private:
			std::vector<Server<K>*> _upstream;
			S _strategy;
			Iprofiler _profiler;
		};

		template<template<class A> class Pool, class K, template<class X> class Server, class U>
		class Common_server: public Server_link<Common_server<Pool, K, Server, U>, Server<K>> {
		public:
			typedef typename U::Profiler  Profiler;
			typedef typename U::Rprofiler Rprofiler;
			template<class A> using Carrier = typename U::template Carrier<A>;

		public:
			explicit Common_server(int cpu):
				_pool(),
				_cpu(cpu),
				_profiler(),
				_thread([this] { this->serve(); })
			{}

			~Common_server() {
				if (!this->stopped()) {
					throw std::logic_error("Server has not yet stopped to be destroyed.");
				}
				std::unique_lock<std::mutex> lock(_mutex);
				while (!_pool.empty()) {
					Carrier<K> carrier = _pool.front();
					_pool.pop();
					if (carrier.get_kernel() != nullptr) {
						delete carrier.get_kernel();
					}
				}
			}

			void send(K* kernel) {
				std::unique_lock<std::mutex> lock(_mutex);
				_pool.push(Carrier<K>(kernel));
				_semaphor.notify_one();
			}

			void wait_impl() {
				if (_thread.joinable())
					_thread.join();
			}

			void* profiler() { return &_profiler; }

			void stop_impl() {
				_semaphor.notify_all();
			}

			void write(std::ostream& out) const { out << name() << ' ' << _cpu; }

		protected:

			virtual const char* name() const = 0;
			virtual void process_kernel(Carrier<K>& carrier) = 0;

			virtual void serve() {
				thread_affinity(_cpu);
				process();
			}

		private:

			void wait_for_a_kernel() {
				std::unique_lock<std::mutex> lock(_mutex);
				_semaphor.wait(lock, [this] {
					return !_pool.empty() || this->stopped();
				});
			}

			void process() {
				while (!this->stopped()) {
					if (!_pool.empty()) {
						std::unique_lock<std::mutex> lock(_mutex);
						Carrier<K> carrier = _pool.front();
						_pool.pop();
						lock.unlock();
						process_kernel(carrier);
					} else {
						wait_for_a_kernel();
					}
				}
			}


		private:
			Pool<Carrier<K>> _pool;
			int _cpu;

		protected:
			Rprofiler _profiler;

		private:
			std::mutex _mutex;
			std::condition_variable _semaphor;
			std::thread _thread;
		};

		template<template<class A> class Pool, class K, template<class X> class Server, class U>
		class Rserver: public Common_server<Pool, K, Server, U> {
		public:
			typedef Common_server<Pool, K, Server, U> Super;
			template<class A> using Carrier = typename U::template Carrier<A>;
//			typedef typename U::template Carrier<K> Carrier;
			typedef typename U::Advisor Advisor;
			explicit Rserver(int cpu): Super(cpu) {}

		protected:

			const char* name() const { return "upstream"; }

			void process_kernel(Carrier<K>& carrier) {
				Advisor advisor;
				if (advisor.is_ready_to_go(carrier, this, this->_profiler)) {
					K* a = carrier.get_kernel();
					this->root()->send(a);
				} else {
					this->_profiler(carrier);
				}
			}

		};

		template<template<class A> class Pool, class K, template<class X> class Server, class U>
		class Downstream_server: public Common_server<Pool, K, Server, U> {
		public:
			typedef Common_server<Pool, K, Server, U> Super;
			template<class A> using Carrier = typename U::template Carrier<A>;
			explicit Downstream_server(int cpu): Super(cpu) {}

		protected:

			const char* name() const { return "downstream"; }

			void process_kernel(Carrier<K>& carrier) {
				K* a = carrier.get_kernel();
				a->act();
				if (a->principal() == nullptr) {
					delete a;
					this->stop();
					std::clog << "SHUTDOWN" << std::endl;
				} else {
					delete a;
				}
			}
		};

		template<template<class A> class Pool, class K, template<class X> class Server, class U>
		class Remote_Iserver: public Common_server<Pool, K, Server, U> {
		public:
			typedef Common_server<Pool, K, Server, U> Super;
			template<class A> using Carrier = typename U::template Carrier<A>;
			typedef typename U::Profiler  Profiler;
			typedef typename U::Iprofiler Iprofiler;

		protected:

			const char* name() const { return "remote_iserver"; }

			void process_kernel(Carrier<K>& carrier) {
				K* k = carrier.get_kernel();
				k->from(_callback_address);
				int i = _strategy(k, _profiler);
				if (i != -1) {
					_upstream[i]->send(k);
				}
			}

		public:

			explicit Remote_Iserver(const std::vector<Server<K>*>& upstream, Endpoint callback, int cpu):
				Super(cpu),
				_upstream(upstream),
				_strategy(upstream),
				_profiler(create_profilers()),
				_callback_address(callback)
			{
				std::for_each(_upstream.begin(), _upstream.end(), [this] (Server<K>* srv) {
					srv->parent(this);
				});
			}

		private:

			std::map<Endpoint, Profiler*> create_profilers() const {
				int idx = 0;
				std::map<Endpoint, Profiler*> profilers;
				std::for_each(_upstream.cbegin(), _upstream.cend(), [&profilers, &idx] (Server<K>* srv) {
					Profiler* prof = static_cast<Profiler*>(srv->profiler());
					prof->index(idx++);
					profilers[prof->endpoint()] = prof;
					std::clog << "Upstream[" << prof->endpoint() << "] = " << prof << std::endl;
				});
				return profilers;
			}

		public:

			~Remote_Iserver() {
				std::for_each(_upstream.begin(), _upstream.end(), [] (Server<K>* srv) {
					delete srv;
				});
			}

			void* profiler() { return &_profiler; }

			void wait_impl() {
				std::for_each(_upstream.begin(), _upstream.end(), std::mem_fn(&Server<K>::wait));
			}

			void stop_impl() {
				std::for_each(_upstream.begin(), _upstream.end(), std::mem_fn(&Server<K>::stop));
			}

			void write(std::ostream& out) const {
				out << "remote_iserver {";
				std::for_each(_upstream.begin(), _upstream.end(), [&out] (Server<K>* srv) {
					out << *srv << ';';
				});
				out << '}';
			}

		private:
			std::vector<Server<K>*> _upstream;
			U _strategy;
			Iprofiler _profiler;
			Endpoint _callback_address;
		};

		template<class K, class U, template<class X> class Server, class Type>
		class Remote_server: public Server<K> {
		public:
			typedef typename U::Profiler  Profiler;
			typedef typename U::Rprofiler Rprofiler;

			Remote_server(const std::string& host, Port port): _host(host), _port(port) {
				_profiler.endpoint(Endpoint(_host, _port));
			}

			explicit Remote_server(const Endpoint& endp): _host(endp.host()), _port(endp.port()) {
				_profiler.endpoint(endp);
			}

			~Remote_server() {
				std::clog << "STOPPED = " << std::boolalpha << this->stopped() << std::endl;
			}

			void send(K* kernel) {
				try {
					Client_socket socket(_host, _port);
					const Type* type = Type::types().lookup(typeid(*kernel));
					if (type == nullptr) {
						std::stringstream msg;
						msg << "Can not find type for typeid = " << typeid(*kernel).name();
						throw Durability_error(msg.str(), __FILE__, __LINE__, __func__);
					}
					Foreign_stream packet;
					packet << type->id();
					kernel->write(packet);
					std::clog << "Buffer size: " << packet.size() << std::endl;
					std::clog << "Sent buffer: " << packet << std::endl;
					socket.send(packet);
					std::clog << "Packet sent: " << packet << std::endl;
				} catch (Connection_error& err) {
					std::clog << Error_message(err, __FILE__, __LINE__, __func__);
					this->root()->send(kernel);
				} catch (Error& err) {
					std::clog << Error_message(err, __FILE__, __LINE__, __func__);
				} catch (std::exception& err) {
					std::clog << String_message(err, __FILE__, __LINE__, __func__);
				} catch (...) {
					std::clog << String_message(UNKNOWN_ERROR, __FILE__, __LINE__, __func__);
				}
			}

			void* profiler() { return &_profiler; }
			void wait_impl() { }
			void stop_impl() { }

			void write(std::ostream& out) const {
				out << "remote_rserver " << _host << ':' << _port;
			}

		private:
			std::string _host;
			Port _port;
			Rprofiler _profiler;
		};

		template<class K, template<class X> class Server>
		class Server_server: public Server_link<Server_server<K, Server>, Server<K>> {
		public:
			explicit Server_server(int cpu):
				_services(),
				mtx(),
				mtx2(),
				_semaphore(),
				_cpu(cpu)
			{}

			virtual ~Server_server() {
				std::unique_lock<std::mutex> lock(mtx);
				for (size_t i=0; i<_services.size(); ++i) {
					if (_services[i].first != nullptr) {
						delete _services[i].first;
					}
					delete _services[i].second;
				}
			}

			void send(K* kernel) {
				std::unique_lock<std::mutex> lock(mtx);
				const size_t n = _services.size();
				_services.push_back(std::make_pair(kernel, new std::thread([this, kernel, n] {
					thread_affinity(_cpu);
					kernel->act();
					_services[n].first = nullptr;
				})));
			}

			void wait_impl() {
				std::unique_lock<std::mutex> lock(mtx2);
				_semaphore.wait(lock, [this] { return this->stopped(); });
				for (size_t i=0; i<_services.size(); ++i) {
					std::thread* t = _services[i].second;
					if (t->joinable()) {
						_services[i].second->join();
					}
//					delete _services[i].first;
				}
			}

			void* profiler() { return nullptr; }

			void stop_impl() {
				std::unique_lock<std::mutex> lock(mtx2);
				for (size_t i=0; i<_services.size(); ++i) {
//					_services[i].second->detach();
					_services[i].first->stop();
				}
				_semaphore.notify_all();
			}

		protected:
			void write(std::ostream& out) const { out << "srvserver " << _cpu; }

		private:
			std::vector<std::pair<K*, std::thread*>> _services;
			std::mutex mtx;
			std::mutex mtx2;
			std::condition_variable _semaphore;
			int _cpu;
		};

		struct Server_stack_end: public virtual Resident {
			void send(){}
			void write(std::ostream&) const {}
			void peer(Resident* peer) { _peer = peer; }
			bool stop() {
				bool ret = Resident::stop();
				if (ret) {
					if (_peer != nullptr) {
						_peer->stop();
					}
				}
				return ret;
			}

		private:
			Resident* _peer;
		};

		template<class K, template<class X>class Server, class Next_server>
		class Server_stack: public Next_server, public Server<K> {
		public:
			using Next_server::send;
			typedef Server_stack<K, Server, Next_server> This;

			template<class ... Args>
			explicit Server_stack(Server<K>* srv, Args* ... servers):
				Next_server(servers...), _srv(srv)
			{
				if (_srv != nullptr) _srv->Server<K>::parent(this);
			}

			void send(K* k) {
//				std::clog << typeid(*k).name() << " --> " << typeid(*_srv).name() << std::endl;
				_srv->send(k);
			}
			void wait_impl() { if (_srv != nullptr) _srv->wait(); }
			void stop_impl() { if (_srv != nullptr) _srv->stop(); }
			void* profiler() { return (_srv != nullptr) ? _srv->profiler() : nullptr; }
			void write(std::ostream& out) const {
				Next_server::write(out);
				if (_srv != nullptr) out << *_srv;
			}

			void wait() {
				Next_server::wait();
				This::wait_impl();
			}
			bool stop() {
				bool ret = Next_server::stop();
				if (ret) {
					This::stop_impl();
				}
				return ret;
			}

			void peer(Resident* srv) { Next_server::peer(srv); }

		private:
			Server<K>* _srv;
		};

//		template<template<class X> class Server, class K, class D, class IO, class S>
//		class Server_stack:
//			public Server_link<Server_stack<Server, K, D, IO, S>, Server<K>>,
//			public Server<D>,
//			public Server<IO>,
//			public Server<S>
//		{
//
//		public:
//			Server_stack():
//				upstream_server(nullptr),
//				downstream_server(nullptr),
//				server_server(nullptr),
//				io_server(nullptr),
//				server_server(nullptr)
//			{}
//
//			Server_stack(Server<K>* a, Server<D>* b, Server<IO>* c, Server<S>* d):
//				upstream_server(a),
//				downstream_server(b),
//				io_server(c),
//				server_server(d)
//			{ parents(); }
//
//			~Server_stack() {
//				delete_server(upstream_server);
//				delete_server(downstream_server);
//				delete_server(io_server);
//				delete_server(server_server);
//			}
//
//			void send(K* a) { upstream_server->send(a); }
//			void send(D* a) { downstream_server->send(a); }
//			void send(IO* a) { io_server->send(a); }
//			void send(S* a) { server_server->send(a); }
//
//			void stop_impl() {
//				stop_if(upstream_server);
//				stop_if(downstream_server);
//				stop_if(io_server);
//				stop_if(server_server);
//			}
//
//			void wait_impl() {
//				wait_if(upstream_server);
//				wait_if(downstream_server);
//				wait_if(io_server);
//				wait_if(server_server);
//			}
//
//			void* profiler() { return nullptr; }
//
//			friend std::istream& operator>>(std::istream& in, Server_stack<Server, K,D,IO,S>& rhs) {
//				std::string cfg;
//				in >> cfg;
//				// TODO Flex & bison parser generator will be helpful here.
//				// Actually, it is not helpful. The server definition can be
//				// easily interpreted using C++ templates without the need
//				// for externa parsing library. If static definition of grammar
//				// is not enough, dynamically constructed templated objects can be
//				// employed so that hierarchy of grammatic expressions can be
//				// restored from the plain text.
//				return in;
//			}
//
//		protected:
//			void write(std::ostream& out) const {
//				out << "local {";
//				write(out, "upstream", upstream_server);
//				write(out, "downstream", downstream_server);
//				write(out, "iostream", io_server);
//				write(out, "srvstream", server_server);
//				out << '}';
//			}
//
//		private:
//
//			void parents() {
//				parent(upstream_server);
//				parent(downstream_server);
//				parent(io_server);
//				parent(server_server);
//			}
//
//			template<class Srv>
//			inline void parent(Srv* srv) {
//				if (srv != nullptr) srv->parent(this);
//			}
//
//			template<class Srv>
//			inline void wait_if(Srv* srv) {
//				if (srv != nullptr) srv->wait();
//			}
//
//			template<class Srv>
//			inline void stop_if(Srv* srv) {
//				if (srv != nullptr) srv->stop();
//			}
//
//			template<class Srv>
//			static void delete_server(Srv* server) {
//				if (server != nullptr) {
//					delete server;
//				}
//			}
//
////			template<template<class X> class Srv, class K1, class K2>
////			static Srv<K1>* real_or_proxy(Srv<K1>* srv, Srv<K2>* rep) {
////				return srv == nullptr ? new Server_stack<K1, K2>(rep) : srv;
////			}
//
//			template<class Srv>
//			static void write(std::ostream& out, const char* name, Srv* srv) {
//				if (srv != nullptr) {
//					out << name << ' ' << *srv << ';';
//				}
//			}
//
//			Server<K>*  upstream_server;
//			Server<D>*  downstream_server;
//			Server<IO>* io_server;
//			Server<S>*  server_server;
//
//		};

	}
}
