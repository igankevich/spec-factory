namespace factory {

	// TODO Add static assertions for non-portable types.
	
	class Foreign_stream {

	public:
		
		typedef uint32_t size_type;

		Foreign_stream(): _buffer(), read_position(0), _declared_size(0) { }

		Foreign_stream& operator<<(char rhs) { return write(rhs); }

		Foreign_stream& operator<<(int8_t rhs)  { return write(rhs); }
		Foreign_stream& operator<<(int16_t rhs) { return write(rhs); }
		Foreign_stream& operator<<(int32_t rhs) { return write(rhs); }
		Foreign_stream& operator<<(int64_t rhs) { return write(rhs); }

		Foreign_stream& operator<<(uint8_t rhs) { return write(rhs); }
		Foreign_stream& operator<<(uint16_t rhs) { return write(rhs); }
		Foreign_stream& operator<<(uint32_t rhs) { return write(rhs); }
		Foreign_stream& operator<<(uint64_t rhs) { return write(rhs); }

		Foreign_stream& operator<<(float rhs) { return write(rhs); }
		Foreign_stream& operator<<(double rhs) { return write(rhs); }
		Foreign_stream& operator<<(long double rhs) { return write(rhs); }

		Foreign_stream& operator<<(const std::string& rhs) { return write(rhs); }

		Foreign_stream& operator>>(char& rhs) { return read(rhs); }

		Foreign_stream& operator>>(int8_t& rhs) { return read(rhs); }
		Foreign_stream& operator>>(int16_t& rhs) { return read(rhs); }
		Foreign_stream& operator>>(int32_t& rhs) { return read(rhs); }
		Foreign_stream& operator>>(int64_t& rhs) { return read(rhs); }

		Foreign_stream& operator>>(uint8_t& rhs) { return read(rhs); }
		Foreign_stream& operator>>(uint16_t& rhs) { return read(rhs); }
		Foreign_stream& operator>>(uint32_t& rhs) { return read(rhs); }
		Foreign_stream& operator>>(uint64_t& rhs) { return read(rhs); }

		Foreign_stream& operator>>(float& rhs) { return read(rhs); }
		Foreign_stream& operator>>(double& rhs) { return read(rhs); }
		Foreign_stream& operator>>(long double& rhs) { return read(rhs); }

		Foreign_stream& operator>>(std::string& rhs) { return read(rhs); }

		size_type size() const { return _buffer.size(); }
		size_type declared_size() const { return _declared_size; }

		void write(const char* bytes, size_type size) {
			size_type idx = _buffer.size();
			try {
				_buffer.resize(idx + size);
			} catch (std::bad_alloc& err) {
				throw Write_error("Writing of a data chunk which does not fit into memory was prevented.",
					__FILE__, __LINE__, __func__);
			}
			std::copy(bytes, bytes + size, &_buffer[idx]);
		}

		void read(char* bytes, size_type size) {
			if (size > _buffer.size() - read_position ) {
				throw Read_error("Reading beyond _buffer limits was prevented.",
					__FILE__, __LINE__, __func__);
			}
			std::copy(&_buffer[read_position], &_buffer[read_position + size], bytes);
			read_position += size;
		}

		void* buffer() { return &_buffer[0]; }

		void insert_size() {
//			size_type packet_size = network_format<size_type, sizeof(size_type)>(_buffer.size());
//			_buffer.insert(_buffer.begin(), (char*)&packet_size, ((char*)&packet_size) + sizeof(size_type));
		}
		
		void extract_declared_size() {
//			size_type sz;
//			std::copy(_buffer.begin(), _buffer.begin() + sizeof(size_type), (char*)&sz);
//			size_type new_size = host_format<size_type, sizeof(size_type)>(sz);
//			if (_declared_size != 0) {
//				std::stringstream msg;
//				msg << "Multiple extraction of declared packet size was prevented.";
//				throw Read_error(msg.str(), __FILE__, __LINE__, __func__);
//			}
//			_declared_size = new_size;
//			_buffer.erase(_buffer.begin(), _buffer.begin() + sizeof(size_type));
		}

		friend std::ostream& operator<<(std::ostream& out, const Foreign_stream& rhs) {
			auto _buffer = rhs._buffer;
			out << std::hex << std::setfill('0');
			for (size_t i=0; i<_buffer.size(); ++i) {
				out << std::setw(2) << (int)_buffer[i];
			}
			out << std::dec << std::setfill(' ');
			return out;
		}

	private:

		template<class T>
		Foreign_stream& write(T rhs) {
			T val = network_format(rhs);
			std::clog << "Converted from " << std::hex << std::setfill('0')
				<< std::setw(sizeof(rhs)*2) << rhs << " to "
				<< std::setw(sizeof(rhs)*2) << val
				<< std::dec << std::endl;
			write((char*)&val, sizeof(rhs));
			return *this;
		}

		Foreign_stream& write(const std::string& rhs) {
			size_type length = rhs.size();
//			std::clog << "Writing string of length = " << length << std::endl;
			write(length);
			write(rhs.c_str(), length*sizeof(std::string::value_type));
			return *this;
		}

		template<class T>
		Foreign_stream& read(T& rhs) {
			T val;
			read((char*)&val, sizeof(rhs));
			rhs = host_format(val);
			std::clog << "Converted from " << std::hex << (uint64_t)val << " to " << (uint64_t)rhs << std::dec << std::endl;
			return *this;
		}

		Foreign_stream& read(std::string& rhs) {
			size_type length;
			read(length);
			std::string::value_type* bytes = new std::string::value_type[length];
//			std::clog << "Reading string of length = " << length << std::endl;
			read(bytes, length);
			rhs.assign(bytes, bytes + length);
			delete[] bytes;
			return *this;
		}

		std::vector<char> _buffer;
		size_type read_position;
		size_type _declared_size;
	};

}
