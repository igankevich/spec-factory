#include <signal.h>
#include <strings.h>
#include <fenv.h>

namespace factory {

	using namespace factory::configuration;

	void emergency_shutdown(int signum);
	
	struct Factory {

		Factory():
			this_server(nullptr),
			remote_server(nullptr)
		{ init_signal_handlers(); }

		~Factory() {
			if (this_server != nullptr) {
				delete this_server;
				this_server = nullptr;
			}
			if (remote_server != nullptr) {
				delete remote_server;
				remote_server = nullptr;
			}
		}

	private:

		void init_signal_handlers() {
			struct sigaction action;
			bzero(&action, sizeof(struct sigaction));
			action.sa_handler = emergency_shutdown;
			::sigaction(SIGTERM, &action, NULL);
			::sigaction(SIGINT, &action, NULL);
			::feenableexcept(FE_INVALID | FE_DIVBYZERO | FE_UNDERFLOW | FE_OVERFLOW);
			::sigaction(SIGFPE, &action, NULL);
		}

		friend Server_stack* the_server();
		friend Server_stack* remote_server();
		friend void construct(Server_stack*, Server_stack*);

		Server_stack* this_server;
		Server_stack* remote_server;

	} bootstrap_factory;

	Server_stack* the_server() { return bootstrap_factory.this_server; }
	Server_stack* remote_server() { return bootstrap_factory.remote_server; }

	void emergency_shutdown(int signal) {
		if (signal == SIGFPE) {
			std::clog << "Floaring point exception caught." << std::endl;
		}
		if (the_server() != nullptr) {
			the_server()->stop();
		}
		if (remote_server() != nullptr) {
			remote_server()->stop();
		}
	}

	void construct(Server_stack* s, Server_stack* r) {
		bootstrap_factory.this_server = s;
		bootstrap_factory.remote_server = r;
		if (bootstrap_factory.this_server != nullptr) {
			bootstrap_factory.this_server->peer(r);
		}
		if (bootstrap_factory.remote_server != nullptr) {
			bootstrap_factory.remote_server->peer(s);
		}
	}

	template<class S, class K>
	std::vector<Server<K>*> server_array(std::size_t size, std::size_t& cpu_id, std::size_t increment = 1) {
		std::vector<Server<K>*> a(size);
		for (std::size_t i=0; i<size; ++i) {
			a[i] = new S(cpu_id);
			cpu_id += increment;
		}
		return a;
	}
}

