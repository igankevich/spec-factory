namespace factory {

	struct Request_handler: public factory::Kernel {

		explicit Request_handler(const Event& e): _event(e) {}

		~Request_handler() {
			if (is_close_event()) {
				std::cout << "Client disconnected on " << _event.data.fd << std::endl;
				::close(_event.data.fd);
			}
		}

		bool is_close_event() const { return (_event.events & EPOLLRDHUP) == EPOLLRDHUP; }
		bool is_read_event() const { return (_event.events & EPOLLIN) == EPOLLIN; }

	protected:
		Event _event;
	};
	
	struct Client_handler: public Request_handler {

		explicit Client_handler(const Event& e):
			Request_handler(e),
			_state(INITIAL)
		{}

		void react(factory::Kernel* k) {

			Request_handler* h = reinterpret_cast<Request_handler*>(k);

//			if (h->is_read_event()) {
			ssize_t cnt = 0;
			while ((cnt = check_read(::read(socket(), _buffer, sizeof(_buffer)))) > 0) {
				packet.write(_buffer, cnt);
			}
//			}

			if (h->is_close_event() && _state != RECEIVED_PACKET) {
				_state = RECEIVED_PACKET;
				packet.extract_declared_size();
				std::clog << "Declared packet size: " << packet.declared_size() << std::endl;
				std::clog << "Packet size: " << packet.size() << std::endl;
				std::clog << "Received buffer: " << packet << std::endl;
				try {
					factory::Kernel* kernel = Type::types().read_object(packet);
					std::clog << "From: " << kernel->from() << std::endl;
					the_server()->send(kernel);
				} catch (Error& err) {
					std::clog << Error_message(err, __FILE__, __LINE__, __func__);
				} catch (std::exception& err) {
					std::clog << String_message(err.what(), __FILE__, __LINE__, __func__);
				} catch (...) {
					std::clog << String_message(UNKNOWN_ERROR, __FILE__, __LINE__, __func__);
				}
				commit(the_server());
			}
		}

		void handle_event(const Event& e) {
			downstream(the_server(), new Request_handler(e));
		}

		Socket socket() const { return Socket(_event.data.fd); }

	private:

		int check_read(int ret) {
//			return (errno == EAGAIN) ? 0 : check("read()", ret);
//			return check("read()", ret);
			return ret;
		}

		static const uint32_t BUFFER_SIZE = 128;
		Foreign_stream packet;
		enum {INITIAL, RECEIVED_PACKET} _state;
		char _buffer[BUFFER_SIZE];
	};
	
	
	struct Network_service: public Service {

		explicit Network_service(const Endpoint& endp):
			_service_socket(endp.host(), endp.port()), _poller(_service_socket) {}

		Network_service(const std::string& host, Port port):
			_service_socket(host, port), _poller(_service_socket) {}

		void act() {
			_poller.run([this] (Event e) {
				Socket client_socket(e.data.fd);
				auto result = workers.find(client_socket);
				if (result == workers.end()) {
					std::clog << "Starting client handler for " << client_socket << std::endl;
					Client_handler* kernel = new Client_handler(e);
					workers[client_socket] = kernel;
					kernel->parent(this);
					the_server()->send(new Kernel_pair(kernel, kernel));
				} else {
					std::clog << "Handling " << client_socket << " with existing client handler" << std::endl;
					Client_handler* kernel = result->second;
					kernel->handle_event(e);
				}
			});
		}

		void react(factory::Kernel* k) {
			Client_handler* h = reinterpret_cast<Client_handler*>(k);
			workers.erase(h->socket());
			std::clog << "Removing " << h->socket() << " from the mapping" << std::endl;
		}

		void stop() {
			_poller.stop();
			std::clog << "STOP NETWORK SERVICE" << std::endl;
		}
	
	private:
		Server_socket _service_socket;
		Event_poller _poller;

		std::unordered_map<Socket, Client_handler*, std::hash<int>> workers;
	};
	
}
