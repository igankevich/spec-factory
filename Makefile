ROOT     = $(PWD)
BIN      = $(ROOT)/bin
TEST     = $(ROOT)/test

BINARY   = $(BIN)/discovery
SOURCES  = discovery.cc
CXX      = g++
CXXFLAGS = -std=c++11 -Wpedantic -Wall -Wextra -gdwarf-3
LDFLAGS  = -lpthread -lz

TIME     = /usr/bin/time -f 'Total time: %e'

$(BINARY): $(SOURCES) *.hh factory/* $(BIN)
	$(CXX) $(CXXFLAGS) $(SOURCES) $(LDFLAGS) -o $(BINARY)

clean:
	rm -f $(BINARY)

run1: $(BINARY) $(TEST)
	@echo Starting server 1...
	@(ulimit -c unlimited; cd $(TEST) && START_ID=1000 $(BINARY) server localhost:10001 localhost:10000)

run2: $(BINARY) $(TEST)
	@echo Starting server 2...
	@(ulimit -c unlimited; cd $(TEST) && START_ID=2000 $(BINARY) server localhost:10002 localhost:10000)

runclient: $(BINARY) $(TEST)
	@echo Starting client...
	@(ulimit -c unlimited; cd $(TEST) && START_ID=1    $(TIME) $(BINARY) client localhost:10000 localhost:10001)

debug: $(BINARY) $(TEST)
	(cd $(TEST) && gdb -q $(BINARY))

$(TEST):
	mkdir $(TEST)

$(BIN):
	mkdir $(BIN)
