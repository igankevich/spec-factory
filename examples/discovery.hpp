void find() {
	struct ifaddrs *ifaddr;
	int family, s;
	char host[NI_MAXHOST];
	char netmask[NI_MAXHOST];

	if (getifaddrs(&ifaddr) == -1) {
		perror("getifaddrs");
		exit(EXIT_FAILURE);
	}

	/* Walk through linked list, maintaining head pointer so we
	      can free list later */

	for (struct ifaddrs* ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
		if (ifa->ifa_addr == NULL
				|| htonl(((struct sockaddr_in*) ifa->ifa_addr)->sin_addr.s_addr) == 0x7f000001ul
				|| ifa->ifa_addr->sa_family != AF_INET)
			continue;

		family = ifa->ifa_addr->sa_family;

		/* Display interface name and family (including symbolic
		      form of the latter for the common families) */

//		printf("%-8s %s (%d)\n",
//			   ifa->ifa_name,
//			   (family == AF_PACKET) ? "AF_PACKET" :
//			   (family == AF_INET) ? "AF_INET" :
//			   (family == AF_INET6) ? "AF_INET6" : "???",
//			   family);

		/* For an AF_INET* interface address, display the address */

		s = getnameinfo(ifa->ifa_addr,
						sizeof(struct sockaddr_in),
						host, NI_MAXHOST,
						NULL, 0, NI_NUMERICHOST);
		if (s != 0) {
			printf("getnameinfo() failed: %s\n", gai_strerror(s));
			exit(EXIT_FAILURE);
		}

		s = getnameinfo(ifa->ifa_netmask,
						(family == AF_INET) ? sizeof(struct sockaddr_in) :
						sizeof(struct sockaddr_in6),
						netmask, NI_MAXHOST,
						NULL, 0, NI_NUMERICHOST);
		if (s != 0) {
			printf("getnameinfo() failed: %s\n", gai_strerror(s));
			exit(EXIT_FAILURE);
		}

		uint32_t addr_long = htonl(((struct sockaddr_in*) ifa->ifa_addr)->sin_addr.s_addr);
		uint32_t mask_long = htonl(((struct sockaddr_in*) ifa->ifa_netmask)->sin_addr.s_addr);

		uint32_t start = (addr_long & mask_long) + 1;
		uint32_t end = (addr_long & mask_long) + (~mask_long);

		printf("\t\taddress: <%s>\n", host);
		printf("\t\tnetmask: <%s>\n", netmask);
		printf("\t\taddr_long: <%x>\n", addr_long);
		printf("\t\tmask_long: <%x>\n", mask_long);
		printf("\t\tshrt_addr: <%x>\n", addr_long & ~mask_long);
		printf("\t\tstrt_addr: <%x>\n", start);
		printf("\t\tend__addr: <%x>\n", end);

		for (uint32_t i=start; i<end; ++i) {
			struct sockaddr_in addr;
			addr.sin_family = AF_INET;
			addr.sin_addr.s_addr = ntohl(i);
			addr.sin_port = 0;
			s = getnameinfo((struct sockaddr*)&addr, sizeof(struct sockaddr_in),
							host, NI_MAXHOST,
							NULL, 0, NI_NUMERICHOST);
			printf("%s\n", host);
		}
		
	}

	freeifaddrs(ifaddr);
}
