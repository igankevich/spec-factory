#include <iostream>
#include <iomanip>
#include <limits>

int main() {
	std::clog << "float: " << std::boolalpha << std::numeric_limits<float>::is_iec559 << std::endl;
	std::clog << "double: " << std::boolalpha << std::numeric_limits<double>::is_iec559 << std::endl;
	std::clog << "long double: " << std::boolalpha << std::numeric_limits<long double>::is_iec559 << std::endl;
	return 0;
}

