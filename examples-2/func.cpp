#include <iostream>
#include <iomanip>
#include <vector>
#include <limits>
#include <typeinfo>

using namespace std;

struct A {
	void read() {
		std::cout << typeid(*this).name() << "::read" << std::endl;
	}
};

template<class T>
struct B: public T {
	void read() {
		T::read();
		std::cout << typeid(*this).name() << "::read" << std::endl;
	}
};

template<class T>
struct C: public T {
};

int main() {
	C<B<A>> c;
	c.read();
	return 0;
}
